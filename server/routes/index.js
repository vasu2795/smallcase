const PortfoliosController = require('../controllers').portfolios;
const TradesController = require('../controllers').trades;

module.exports = (app) => {
  app.get('/', (req, res) => res.status(200).send({
    message: 'Welcome to the Portfolio API!',
  }));

  app.get('/portfolios', PortfoliosController.list);
  app.get('/portfolios/:portfolioId', PortfoliosController.retrieve);
  app.get('/portfolios/:portfolioId/holdings', PortfoliosController.holdings);
  app.get('/portfolios/:portfolioId/getReturns', PortfoliosController.returns);

  app.post('/stocks/:stockId/addTrade', TradesController.create);
  app.post('/stocks/:stockId/updateTrade/:tradeId', TradesController.update);
  app.post('/stocks/:stockId/removeTrade/:tradeId', TradesController.remove);
};
