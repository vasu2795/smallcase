module.exports = (sequelize, DataTypes) => {
  const trades = sequelize.define('trades', {
    type: {
      type: DataTypes.STRING,
      allowNull: false
    },
    quantity: {
      type: DataTypes.STRING,
      allowNull: false
    },
    rate: {
      type: DataTypes.STRING,
      allowNull: false
    },
    date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    stockId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    }
  },{
    timestamps: false
  });
  trades.associate = (models) => {
    trades.belongsTo(models.portfolio, {
      foreignKey: 'stockId',
      onDelete: 'CASCADE',
    });
  };
  return trades;
};
