module.exports = (sequelize, DataTypes) => {
  const portfolio = sequelize.define('portfolio', {
    portfolio: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    }
  },{
    timestamps: false
  });
  portfolio.associate = (models) => {
    portfolio.hasMany(models.stocks, {
      foreignKey: 'portfolioId',
      as: 'stocks',
    });
  };
  return portfolio;
};
