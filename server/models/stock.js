module.exports = (sequelize, DataTypes) => {
  console.log("stocks here..")
  const stocks = sequelize.define('stocks', {
    stock: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {
    timestamps: false
  });
  stocks.associate = (models) => {
    stocks.belongsTo(models.portfolio, {
      foreignKey: 'portfolioId',
      onDelete: 'CASCADE',
    });
    stocks.hasMany(models.trades, {
      foreignKey: 'stockId',
      as: 'trades',
    });

  };
  return stocks;
};
