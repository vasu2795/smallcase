const ErrorMap = (error) => {
    return { success: false, data: error } // we can add a JSON error mapping if necessary
}

const SuccessMap = (data) => {
    return { success: true, data: data }
}

var isDate = function(date) {
    return (new Date(date) !== "Invalid Date") && !isNaN(new Date(date));
}

const isValidAddTrade = (req) => {
  let rate =  Number.isInteger(req.body.rate);
  let stockId=  Number.isInteger(parseInt(req.params.stockId));
  let quantity=  Number.isInteger(req.body.quantity);
  let type=  (["Buy","Sell"].indexOf(req.body.type) > -1 ? true : false);
  let date= isDate(new Date(req.body.date));
  return (rate && stockId && quantity && type && date)
}

const isValidUpdateTrade = (req) => {
  let valid = true;
  let rate = req.body.rate ?  Number.isInteger(req.body.rate) : valid;
  let stockId=  Number.isInteger(parseInt(req.params.stockId));
  let quantity=  req.body.rate ?  Number.isInteger(req.body.rate) : valid;;
  let type=  req.body.type ? (["Buy","Sell"].indexOf(req.body.type) > -1 ? true : false) : valid;
  let date= req.body.date ? isDate(new Date(req.body.date)) : valid;
  return (rate && stockId && quantity && type && date)
}


exports.ErrorMap = ErrorMap;
exports.SuccessMap = SuccessMap;
exports.isValidAddTrade = isValidAddTrade;
exports.isValidUpdateTrade = isValidUpdateTrade;
