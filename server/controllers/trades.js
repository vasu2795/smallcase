const portfolio = require('../models').portfolio;
const stocks = require('../models').stocks;
const trades = require('../models').trades;
const dbConfig = require("../models/index.js");
const R = require('ramda');
const axios = require('axios');
const ErrorMap = require("../utils/utils.js").ErrorMap
const SuccessMap = require("../utils/utils.js").SuccessMap
const isValidAddTrade = require("../utils/utils.js").isValidAddTrade
const isValidUpdateTrade = require("../utils/utils.js").isValidUpdateTrade

String.prototype.float = function() {
  return parseFloat(this.replace(',', ''));
}

module.exports = {
  create(req, res) {

    if(!isValidAddTrade(req))
      return res.status(404).send(ErrorMap("Invalid Params"));

    console.log("here...");
    return trades
      .create({
        rate: req.body.rate,
        stockId: parseInt(req.params.stockId),
        quantity: req.body.quantity,
        type: req.body.type,
        date: new Date(req.body.date)
      })
      .then(trade => res.status(201).send(SuccessMap(trade)))
      .catch(error => res.status(400).send(ErrorMap(error)));
  },

  update(req, res) {

    if(!isValidUpdateTrade(req))
      return res.status(404).send(ErrorMap("Invalid Params"));

    return trades
      .find({
        where: {
          id: req.params.tradeId,
          stockId: req.params.stockId,
        },
      })
      .then(trade => {
        if (!trade) {
          return res.status(404).send(ErrorMap('trade Not Found'));
        }

        return trade
          .update({
            rate: req.body.rate ? req.body.rate : trade.rate,
            quantity: req.body.quantity  ? req.body.quantity : trade.quantity,
            type: req.body.type ? req.body.type : trade.type,
            date: req.body.date ? new Date(req.body.date) : trade.date
          })
          .then(updatedTtrade => res.status(200).send(updatedTtrade))
          .catch(error => res.status(400).send(ErrorMap(error)));
      })
      .catch(error => res.status(400).send(ErrorMap(error)));
  },

  remove(req, res) {
    return trades
      .find({
        where: {
          id: req.params.tradeId,
          stockId: req.params.stockId,
        },
      })
      .then(trade => {
        if (!trade) {
          return res.status(404).send(ErrorMap('trade Not Found'));
        }

        return trade
          .destroy()
          .then(() => res.status(200).send(SuccessMap("Deleted Trade Record")))
          .catch(error => res.status(400).send(ErrorMap(error)));
      })
      .catch(error => res.status(400).send(ErrorMap(error)));
  },
};
