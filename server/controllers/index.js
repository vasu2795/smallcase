const portfolios = require('./portfolios');
const trades = require('./trades');

module.exports = {
  portfolios,
  trades
};
