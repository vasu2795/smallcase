const portfolio = require('../models').portfolio;
const stocks = require('../models').stocks;
const trades = require('../models').trades;
const dbConfig = require("../models/index.js");
const R = require('ramda');
const axios = require('axios');
const ErrorMap = require("../utils/utils.js").ErrorMap
const SuccessMap = require("../utils/utils.js").SuccessMap
var googleStocks = require('google-stocks');

String.prototype.float = function() {
  return parseFloat(this.replace(',', ''));
}

const calcPerShareOrig = stock => {
    let trades = stock["trades"];
    if(trades == [])  // no trades for the given stock !
      return 0;

    // adding amount = rate *  quantity to each column
    let tradesWithAmount = R.map(function(trade) {
                                   return {type: trade.type, amount: trade.rate * trade.quantity }
                                 },trades);

    // cumulative amount invested so far = total Buy amount - total sell amount
    let totalAmount = R.reduce(function(a,b) {
                                return {amount: b.type=="Buy" ? a.amount + b.amount : a.amount - b.amount }
                              },{amount: 0},tradesWithAmount).amount;


    // cumulative count of shares held = total buy - total sell
    let totalQuantity = R.reduce(function(a,b) { return { quantity:  b.type=="Buy" ? a.quantity + b.quantity :  a.quantity - b.quantity}},{ quantity: 0},trades).quantity;

    return parseFloat(totalAmount/totalQuantity);  // return effective per share original price = cumulative amount invested / cumulative shares held
}

const calculateReturns = (currPrice,stocks) => {
  return R.addIndex(R.map)(function(stock,index) {
                var stockName = stock["stock"];
                var currPerSharePrice = currPrice[index].l.float();
                var origPerSharePrice = calcPerShareOrig(stock);
                let returns = origPerSharePrice ? (parseFloat((currPerSharePrice - origPerSharePrice)/origPerSharePrice) * 100).toFixed(2) : 0
                console.log("returns  ",stockName,currPerSharePrice,origPerSharePrice,returns)
                return {stock: stockName,currPerSharePrice: currPerSharePrice, origPerSharePrice: origPerSharePrice, returns: " " + returns + " %"}
              },stocks);
}


const getHolding = function(stock) {
    let trades = stock["trades"];
    if(trades == [])  // no trades for the given stock !
      return 0;

    // adding amount = rate *  quantity to each column
    let tradesWithAmount = R.map(function(trade) {
                                   return {amount: trade.rate * trade.quantity, type: trade.type }
                                 },trades);

    // total buy amount
    var totalAmount = R.reduce(function(a,b) {  console.log("a and b are " ,a,b); return { amount: b.type=="Buy" ? a.amount + b.amount : a.amount }},{ amount: 0, type: "Buy"},tradesWithAmount).amount;

    // total 'buy'
    var totalBuys = R.reduce(function(a,b) { return { quantity:  b.type=="Buy" ? a.quantity + b.quantity :  a.quantity}},{ quantity: 0},trades).quantity
    var totalQuantity = R.reduce(function(a,b) { return { quantity:  b.type=="Buy" ? a.quantity + b.quantity :  a.quantity - b.quantity}},{ quantity: 0},trades).quantity;

    var hold =  parseFloat(totalAmount)/parseFloat(totalBuys);

    return {
      "stock": stock["stock"],
      "holding": (totalQuantity > 0 ? totalQuantity : 0) + " @ " + hold
    }
}


module.exports = {
  create(req, res) {
    return portfolio
      .create({
        portfolio: req.body.portfolio,
      })
      .then((portfolio) => res.status(201).send(SuccessMap(portfolio)))
      .catch((error) => res.status(400).send(ErrorMap(error)));
  },

  list(req, res) {
    return portfolio
      .findAll({
        include: [{
          model: stocks,
          include: [ {model: trades, as: 'trades'}],
          as: 'stocks',
        }]
      })
      .then((portfolio) => res.status(200).send(SuccessMap(portfolio)))
      .catch((error) => res.status(400).send(ErrorMap(error)));
  },

  retrieve(req, res) {
    return portfolio
      .findById(req.params.portfolioId, {
        include: [{
          model: stocks,
          include: [ {model: trades, as: 'trades'}],
          as: 'stocks',
        }]
      })
      .then((portfolio) => {
        if (!portfolio) {
          return res.status(404).send(ErrorMap('Portfolio Not Found'))
        }
        return res.status(200).send(SuccessMap(portfolio));
      })
      .catch((error) => res.status(400).send(ErrorMap(error)));
  },

  holdings(req, res) {
    return portfolio
    .findById(req.params.portfolioId, {
      include: [{
        model: stocks,
        include: [ {model: trades,as: 'trades' }],
        as: 'stocks',
      }]
    })
    .then((portfolio) => {
      if (!portfolio) {
        return res.status(404).send(ErrorMap('Portfolio Not Found'))
      }
      var data = R.map(getHolding,portfolio["stocks"]);
      res.status(200).send(SuccessMap(data));
    })
    .catch((error) => res.status(400).send(ErrorMap(error)));
  },

  returns(req, res) {
    return portfolio
    .findById(req.params.portfolioId, {
      include: [{
        model: stocks,
        include: [ {model: trades, attributes: ['type','rate','quantity'], as: 'trades' }],
        as: 'stocks',
      }]
    })
    .then((portfolio) => {
      if (!portfolio) {
        return res.status(404).send(ErrorMap('Portfolio Not Found'))
      }
      googleStocks(R.map(function(a) { return a.stock},portfolio["stocks"]))
      .then(function(data) {
         var returns = calculateReturns(data,portfolio["stocks"]);
         return res.status(200).send(SuccessMap(returns));
      })
      .catch(function(error) {
        return res.status(400).send(ErrorMap(error))
      });
    })
    .catch((error) => res.status(400).send(ErrorMap(error)));
  },
};
